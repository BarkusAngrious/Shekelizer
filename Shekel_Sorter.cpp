//4Chan thread parser
//NrTox.3

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

//Global settings.
int settings_Newline;
int settings_Thread_Cap;
int settings_Min_Thread_Length;
int settings_Output_Format;

//The topic of the thread, loaded form the post object so it is up here.
string Topic;

//Loads the settings from settings.cfg
void load_Settings()
{
     ifstream SF;
     string tmp_Setting_Name = "";
     
     if (!SF.is_open())
     {
          settings_Newline = 0;
          settings_Min_Thread_Length = 3;
          settings_Thread_Cap = 500;
          settings_Output_Format = 1;
     }
     
     SF.open("Shekel_Sorter.cfg");
     
     SF >> tmp_Setting_Name;
     SF >> settings_Newline;
     
     SF >> tmp_Setting_Name;
     SF >> settings_Thread_Cap;
     
     SF >> tmp_Setting_Name;
     SF >> settings_Min_Thread_Length;
     
     SF >> tmp_Setting_Name;
     SF >> settings_Output_Format;
     
     SF.close();
}

//Allows the deletion of a portion of the given string.
string eviscerate_String(string p_String, int p_Start, int p_Stop, string p_Replacement = "NO_DATA")
{
     string tmp_String = "";
     int tmp_Size = (int (p_String.size()));
     
     for (int cou_Index=0;cou_Index<p_Start;cou_Index++)
     {
          if (cou_Index >= tmp_Size){ return tmp_String; }
          
          tmp_String += p_String[cou_Index];
     }
     
     if (p_Replacement != "NO_DATA")
     {
          tmp_String += p_Replacement;
     }
     
     for (int cou_Index=p_Stop;cou_Index<tmp_Size;cou_Index++)
     {
          if (cou_Index >= tmp_Size){ return tmp_String; }
          
          tmp_String += p_String[cou_Index];
     }
     return tmp_String;
}


//The data type used for internal manipulation.
union u_GData
{
     int I;
     float F;
     char C;
     bool B;
     void * NR;
};

//For the lookup tree I will be using get() and set() functions to set and retrieve data as the array holding it will be dynamic.
class c_TP_Variable_Node
{
     friend class c_TP_Lookup_Tree; 
private:
     
     c_TP_Variable_Node * Right;
     c_TP_Variable_Node * Left;
     
     
     u_GData * Data;
     int Data_Length;
     int Var_Type;
          
public:
     
     c_TP_Variable_Node()
     {
          Data = NULL;
          Var_Type = 0;
          Data_Length = 0;
          Right = NULL;
          Left = NULL;
     }
     
     ~c_TP_Variable_Node()
     {
          delete [] Data;
          Data = NULL;
          
          delete Right;
          Right = NULL;
          
          delete Left;
          Left = NULL;
     }
     
     string Var_Name;
     
     //--     SET FUNCTIONS FOR THE DATA
     
     //String
     void set_string(string p_Data)
     {
          delete [] Data;
          Data = new u_GData[p_Data.size()];
          
          //cout << "\n\n\t Setting Characters->";
          for (unsigned int cou_Index=0;cou_Index<p_Data.size();cou_Index++)
          {
               Data[cou_Index].I = int (p_Data[cou_Index]);
               //cout << "\n\t\t" << Data[cou_Index].I;
          }
          //cout << "\n\t Finished Gathering...";
          Var_Type = 0;
          Data_Length = p_Data.size();
          
          //cout << "\n\t\t\t set_string(" << p_Data << ")";
     }
     void set_int(int p_Data)          { Data = new u_GData[1]; Data[0].I =  p_Data; Var_Type = 1; Data_Length = 1; }// cout << "\n\t\t\t set_int(" << p_Data << ")"; }
     void set_float(float p_Data)      { Data = new u_GData[1]; Data[0].F =  p_Data; Var_Type = 2; Data_Length = 1; }// cout << "\n\t\t\t set_float(" << p_Data << ")"; }
     void set_reference(void * p_Data) { Data = new u_GData[1]; Data[0].NR = p_Data; Var_Type = 3; Data_Length = 1; }// cout << "\n\t\t\t set_reference(" << p_Data << ")"; }
     void set_bool(bool p_Data)        { Data = new u_GData[1]; Data[0].B =  p_Data; Var_Type = 4; Data_Length = 1; }// cout << "\n\t\t\t set_bool(" << p_Data << ")"; }
     
     void set_raw(string p_Var_Name, u_GData p_Data[], int p_Data_Length, int p_Data_Type)
     {
          delete [] Data;
          Data = new u_GData[p_Data_Length];
          Data_Length = p_Data_Length; 
          Var_Type = p_Data_Type;
          Var_Name = p_Var_Name;
          
          for (int cou_Index=0;cou_Index<Data_Length;cou_Index++)
          {
               Data[cou_Index].NR = p_Data[cou_Index].NR;
          }
     }
     
     
     //--     GET FUNCTIONS FOR THE DATA
     
     string get_string()
     {    
          if (Data == NULL){ return "";   } 
          string tmp_String = ""; 
          
          //cout << "\n\n\t Gathering Characters->";
          for (int cou_Index=0;cou_Index<Data_Length;cou_Index++)
          {
               //cout << "\n\t\t" << Data[cou_Index].I;
               tmp_String += char (Data[cou_Index].I);
          }
          //cout << "\n\t Finished Gathering...";
          
          
          
          //cout << "\n\n\t [" << tmp_String << "]";
          
          
          return tmp_String;
     }
     
     int    get_int(){       if (Data == NULL){ return 0;    } return Data[0].I;}
     float  get_float(){     if (Data == NULL){ return 0.0;  } return Data[0].F;}
     void * get_reference(){ if (Data == NULL){ return NULL; } return Data[0].NR;}
     bool   get_bool(){      if (Data == NULL){ return 0;    } return Data[0].B;}
     
     //Outputs the current node.
     void output_Node()
     {
          cout << "\n\t\t " << this << " Var_Name->" << Var_Name << " Var_Type->" << Var_Type << "  Data_Length->" << Data_Length << "  Data->";
          if (Var_Type == 0){ cout << get_string(); }
          if (Var_Type == 1){ cout << get_int(); }
          if (Var_Type == 2){ cout << get_float(); }
          if (Var_Type == 3){ cout << get_reference(); }
          if (Var_Type == 4){ cout << get_bool(); }
     }
};

class c_TP_Lookup_Tree
{
     private:
             
     public:
          c_TP_Variable_Node * Root;
          c_TP_Variable_Node ** Current;//the current node that will allow referencing outside of the search function
          bool flg_Foundit;//the flag that set when a node is found already set
          
          c_TP_Lookup_Tree()
          {
               Root = NULL;
               Current = NULL;
               flg_Foundit = false;
          }
          
          ~c_TP_Lookup_Tree()
          {
               delete Root;
               //Current does not get deleted as it only points to other nodes that may not exist after root is deleted and the node chain is gone.
               //delete *Current;
          }
          
          int search(string p_Var_Name)
          {
               flg_Foundit = false;
               return query_Name(Root, p_Var_Name);
          }
          
          int query_Name(c_TP_Variable_Node *&p_Node, string p_Var_Name)
          {
               if (p_Node == NULL)
               {
                    p_Node = new c_TP_Variable_Node;
                    p_Node->Right = NULL;
                    p_Node->Left = NULL;
                    p_Node->Var_Name = p_Var_Name;
                    p_Node->set_int(-1);
                    Current = &p_Node;
                    //cout << "\n\n\t New Variable Created";
                    return 0;
               }
               
               if (p_Node->Var_Name == p_Var_Name)
               {
                    Current = &p_Node;  
                    flg_Foundit = true;
                    return p_Node->Var_Type;
               }
                    
               if (p_Var_Name < p_Node->Var_Name)
               {
                    query_Name(p_Node->Left, p_Var_Name);
               }
                    
               if (p_Var_Name > p_Node->Var_Name)
               {
                    query_Name(p_Node->Right, p_Var_Name);
               }
                    
               return 0;
          }
          
          int exists(string p_Var_Name)
          {
               return exists_query_Name(Root, p_Var_Name);
          }
          
          int exists_query_Name(c_TP_Variable_Node *&p_Node, string p_Var_Name)
          {
               if (p_Node == NULL)
               {
                    //cout << "\n\t Variable NOT Found->" << p_Var_Name;
                    return 0;
               }
               
               if (p_Node->Var_Name == p_Var_Name)
               {
                    //cout << "\n\t Variable Found->" << p_Var_Name;
                    return 1;
               }
                    
               if (p_Var_Name < p_Node->Var_Name)
               {
                    return exists_query_Name(p_Node->Left, p_Var_Name);
               }
                    
               if (p_Var_Name > p_Node->Var_Name)
               {
                    return exists_query_Name(p_Node->Right, p_Var_Name);
               }
                    
               return 0;
          }
          
          
          void * get_Bin_Node_Reference(string p_Var_Name)
          {
               flg_Foundit = false;
               return get_Bin_Node_Reference(Root, p_Var_Name);
          }
          
          void * get_Bin_Node_Reference(c_TP_Variable_Node *&p_Node, string p_Var_Name)
          {
               if (p_Node == NULL)
               {
                    return NULL;
               }
               
               if (p_Node->Var_Name == p_Var_Name)
               {
                    cout << "\n\n\t Returning Address " << p_Node;
                    return ((void*) p_Node);
               }
                    
               if (p_Var_Name < p_Node->Var_Name)
               {
                    return get_Bin_Node_Reference(p_Node->Left, p_Var_Name);
               }
                    
               if (p_Var_Name > p_Node->Var_Name)
               {
                    return get_Bin_Node_Reference(p_Node->Right, p_Var_Name);
               }
                    
               return NULL;
          }
          
          int submit_Old_Node(c_TP_Variable_Node *&p_Node)
          {
               return find_Spot_For_Old_Node(Root, p_Node);
          }
          
          int find_Spot_For_Old_Node(c_TP_Variable_Node *&p_Node, c_TP_Variable_Node *&p_Old_Node)
          {
               if (p_Node == NULL)
               {
                    p_Node = p_Old_Node;
                    Current = &p_Node;
                    return 0;
               }
               
               if (p_Node->Var_Name == p_Old_Node->Var_Name)
               {
                    Current = &p_Node;  
                    flg_Foundit = true;
                    
                    cout << "\n\n\t During Replacement A Node With The Same Variable Name Was Found.";
                    
                    return p_Node->Var_Type;
               }
                    
               if (p_Old_Node->Var_Name < p_Node->Var_Name)
               {
                    find_Spot_For_Old_Node(p_Node->Left, p_Old_Node);
               }
                    
               if (p_Old_Node->Var_Name > p_Node->Var_Name)
               {
                    find_Spot_For_Old_Node(p_Node->Right, p_Old_Node);
               }
                    
               return 0;
          }
          
          int null_Leg(c_TP_Variable_Node *&p_Node, string p_Var_Name)
          {
               if (p_Node == NULL)
               {
                    return 0;
               }
               
               if (p_Node->Var_Name == p_Var_Name)
               {
                    return 0;
               }
                    
               if (p_Var_Name < p_Node->Var_Name)
               {
                    if ((p_Node->Left)->Var_Name == p_Var_Name)
                    {
                         delete p_Node->Left;
                         p_Node->Left = NULL;
                    }
                    
                    null_Leg(p_Node->Left, p_Var_Name);
               }
                    
               if (p_Var_Name > p_Node->Var_Name)
               {
                    if ((p_Node->Right)->Var_Name == p_Var_Name)
                    {
                         delete p_Node->Right;
                         p_Node->Right = NULL;
                    }
                    
                    null_Leg(p_Node->Right, p_Var_Name);
               }
                    
               return 0;
          }
          
          void remove_Node(c_TP_Variable_Node *&p_Node)
          {
               //Get the Right leg and set the passed nodes right leg to NULL to avoid deletion.
               c_TP_Variable_Node * tmp_Right = p_Node->Right;
               p_Node->Right = NULL;
               
               //Get the left leg and set the given nodes to NULL.
               c_TP_Variable_Node * tmp_Left = p_Node->Left;
               p_Node->Left = NULL;
               
               //NULL the leg containing the passed node.
               null_Leg(Root, p_Node->Var_Name);
               
               //Submits a node that is already created to the tree.
               //Make shure that it is allocated in the first place, pass a NULL and receive and error.
               if (tmp_Left){ submit_Old_Node(tmp_Left); }
               if (tmp_Right){ submit_Old_Node(tmp_Right); }
          }
          
          void remove_Node(string p_Var_Name)
          {
               search(p_Var_Name);
               remove_Node(*Current);
          }
          
          void output_Tree()
          {
               output_Node(Root);
          }
          
          void output_Node(c_TP_Variable_Node *&p_Node)
          {
               if (p_Node == NULL){ return; }
               output_Node(p_Node->Left);
               p_Node->output_Node();
               output_Node(p_Node->Right);
          }
          
          void output_Current_Node()
          {
               if (Current == NULL){ return; }
               output_Node(*Current);
          }
          
          
          void funger_Tree(void (*p_Function(string)))
          {
               funger_Node(Root, p_Function);
          }
          
          void funger_Node(c_TP_Variable_Node *&p_Node, void (*p_Function(string)))
          {
               if (p_Node == NULL){ return; }
               funger_Node(p_Node->Left, p_Function);
               (*p_Function)(p_Node->Var_Name);
               funger_Node(p_Node->Right, p_Function);
          }
          
          //Gets the data type from the current node after searching the tree.
          int get_Var_Type(string p_Var_Name)            { search(p_Var_Name); return get_Var_Type(*Current);    }
          int get_Var_Type(c_TP_Variable_Node * &p_Node){ if (p_Node != NULL){ return p_Node->Var_Type; } else { return 0; } }
          
          
          //Changes the name of a registered variable, if it already exists then I dunno, don't do that.
          string change_name(string p_Var_Name, string p_Var_New_Name){ search(p_Var_Name); change_name(*Current, p_Var_New_Name); return "1"; }
          string change_name(c_TP_Variable_Node * &p_Node, string p_Var_New_Name){ p_Node->Var_Name = p_Var_New_Name; return "1"; }
          
          
          //Gets the data from the current node after searching the tree.
          string get_string(string p_Var_Name)    { search(p_Var_Name); return get_string(*Current);    }
          int    get_int(string p_Var_Name)       { search(p_Var_Name); return get_int(*Current);       }
          float  get_float(string p_Var_Name)     { search(p_Var_Name); return get_float(*Current);     }
          void * get_reference(string p_Var_Name) { search(p_Var_Name); return get_reference(*Current); }
          bool   get_bool(string p_Var_Name)      { search(p_Var_Name); return get_bool(*Current);      }
          
          string get_string(c_TP_Variable_Node * &p_Node)    { if (p_Node != NULL){ return p_Node->get_string();    } else { return "";    } }
          int    get_int(c_TP_Variable_Node * &p_Node)       { if (p_Node != NULL){ return p_Node->get_int();       } else { return -1;     } }
          float  get_float(c_TP_Variable_Node * &p_Node)     { if (p_Node != NULL){ return p_Node->get_float();     } else { return -1.0;   } }
          void * get_reference(c_TP_Variable_Node * &p_Node) { if (p_Node != NULL){ return p_Node->get_reference(); } else { return NULL;  } }
          bool   get_bool(c_TP_Variable_Node * &p_Node)      { if (p_Node != NULL){ return p_Node->get_bool();      } else { return false; } }
          
          
          //Searches for the variable name given, if not found a node is created for it. After establishing which one the node is the data is set.
          void set_string(string p_Var_Name, string p_Data)    { search(p_Var_Name); set_string(*Current, p_Data);    return; }
          void set_int(string p_Var_Name, int p_Data)          { search(p_Var_Name); set_int(*Current, p_Data);       return; }
          void set_float(string p_Var_Name, float p_Data)      { search(p_Var_Name); set_float(*Current, p_Data);     return; }
          void set_reference(string p_Var_Name, void * p_Data) { search(p_Var_Name); set_reference(*Current, p_Data); return; }
          void set_bool(string p_Var_Name, bool p_Data)        { search(p_Var_Name); set_bool(*Current, p_Data);      return; }
          
          void set_string(c_TP_Variable_Node * &p_Node, string p_Data)    { if (p_Node != NULL){ p_Node->set_string(p_Data);    } }
          void set_int(c_TP_Variable_Node * &p_Node, int p_Data)          { if (p_Node != NULL){ p_Node->set_int(p_Data);       } }
          void set_float(c_TP_Variable_Node * &p_Node, float p_Data)      { if (p_Node != NULL){ p_Node->set_float(p_Data);     } }
          void set_reference(c_TP_Variable_Node * &p_Node, void * p_Data) { if (p_Node != NULL){ p_Node->set_reference(p_Data); } }
          void set_bool(c_TP_Variable_Node * &p_Node, bool p_Data)        { if (p_Node != NULL){ p_Node->set_bool(p_Data);      } }
          
                    
};






//Holds a post.
class c_Post
{
public:
     
     string raw_Post_Data;
     
     string TOPIC;
     string NO;
     string NOW;
     string NAME;
     string COM;
     string FILENAME;
     string URL;
     string EXT;
     string W;
     string H;
     string TN_W;
     string TN_H;
     string TIM;
     string TIME;
     string MD5;
     string FSIZE;
     string RESTO;
     string ID;
     string COUNTRY;
     string COUNTRY_NAME;
     string M_IMG;
     string BUMPLIMIT;
     string IMAGELIMIT;
     string REPLIES;
     string IMAGES;
     string UNIQUE_IPS;
     string TAIL_SIZE;
     
     string * Cited;
     int Cited_Count;
     
     string * Replies;
     int Replies_Count;
     
     //For tag parsing.
     //Tags are separated by "," and ":" separates the tag from the data.
     string tmp_Tag;
     string tmp_Tag_Data;
     int tmp_EIP;
     char tmp_Chunk[3];
     
     int Post_ID;
     
     c_Post()
     {
          raw_Post_Data = "";
          TOPIC = "";
          NO = "";
          NOW = "";
          NAME = "";
          COM = "";
          FILENAME = "";
          EXT = "";
          W = "";
          H = "";
          TN_W = "";
          TN_H = "";
          TIM = "";
          TIME = "";
          MD5 = "";
          FSIZE = "";
          RESTO = "";
          ID = "1234abcd";
          COUNTRY = "";
          COUNTRY_NAME = "";
          M_IMG = "";
          BUMPLIMIT = "";
          IMAGELIMIT = "";
          REPLIES = "";
          IMAGES = "";
          UNIQUE_IPS = "";
          TAIL_SIZE = "";
          
          Cited = NULL;
          Cited_Count = 0;
          
          tmp_Tag = "";
          tmp_Tag_Data = "";
          tmp_EIP = 0;
          tmp_Chunk[0] = ' ';
          tmp_Chunk[1] = ' ';
          tmp_Chunk[2] = ' ';
          
          Post_ID = 0;
     }
     
     ~c_Post()
     {
          if (Cited != NULL){ delete [] Cited; }
          Cited = NULL;
     }
     
     //Adds a citation into the citation array.
     void add_Citation(string p_Citation)
     {
          //Setup the tmp array.
          string * tmp_Cited = new string[Cited_Count];
          
          //Copy old data into it.
          for (int cou_Index=0;cou_Index<Cited_Count;cou_Index++)
          {
               tmp_Cited[cou_Index] = Cited[cou_Index];
          }
          
          //Reallocate the old array.
          if (Cited != NULL){ delete [] Cited; }
          Cited = new string[Cited_Count + 1];
          
          //Copy old data back into the newly created array.
          for (int cou_Index=0;cou_Index<Cited_Count;cou_Index++)
          {
               Cited[cou_Index] = tmp_Cited[cou_Index];
          }
          
          //Destroy tmp_Array.
          if (tmp_Cited != NULL){ delete [] tmp_Cited; }
          tmp_Cited = NULL;
          
          //Set the newest reply.
          Cited[Cited_Count] = p_Citation;
          Cited_Count++;
     }
     
     int is_Num(char p_Num)
     {
          if ((p_Num >= 48) && (p_Num <= 57))
          {
               return 1;
          }
          return 0;
     }
     
     //Gets the tag label and information.
     //Tags are separated by "," and ":" separates the tag from the data.
     int get_Next_Tag()
     {
          tmp_Tag = "";
          tmp_Tag_Data = "";
          char tmp_Delim = ' ';
          int tmp_Flag = 0;
          
          while(!((tmp_Chunk[0] == '\"') && (tmp_Chunk[1] == ',') && (tmp_Chunk[2] == '\"')) && !((tmp_Chunk[0] == ' ') && (tmp_Chunk[1] == ' ') && (tmp_Chunk[2] == '\"')) && !((tmp_Chunk[1] == ',') && (tmp_Chunk[2] == '\"')))
          {
               //Get previous character.
               tmp_Chunk[0] = tmp_Chunk[1];
               tmp_Chunk[1] = tmp_Chunk[2];
               
               //Get current character.
               tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
               
               tmp_EIP++;
               if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
          }
          
          
          //Get previous character.
          tmp_Chunk[0] = tmp_Chunk[1];
          tmp_Chunk[1] = tmp_Chunk[2];
          
          //Get current character.
          tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
          tmp_EIP++;
          if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
               
          while((tmp_Chunk[2] != '\"') && (tmp_Chunk[2] != '['))
          {
               //Check for delim
               if (!((tmp_Chunk[0] == '\"') && (tmp_Chunk[1] == ':') && (tmp_Chunk[2] == '\"')) && !((tmp_Chunk[0] == '\"') && (tmp_Chunk[1] == ':') && (tmp_Chunk[2] == '[')))
               {
                    tmp_Tag += tmp_Chunk[2];
               }
               
               //Get previous character.
               tmp_Chunk[0] = tmp_Chunk[1];
               tmp_Chunk[1] = tmp_Chunk[2];
               
               //Get current character.
               tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
               
               
               tmp_EIP++;
               if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
          }
          
          //Get previous character.
          tmp_Chunk[0] = tmp_Chunk[1];
          tmp_Chunk[1] = tmp_Chunk[2];
          
          //Get current character.
          tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
          tmp_EIP++;
          if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
          
          tmp_Flag = 1;
          if (tmp_Tag == "no"){ tmp_Flag = 0; }
          if (tmp_Tag == "date"){ tmp_Flag = 0; }
          if (tmp_Tag == "w"){ tmp_Flag = 0; }
          if (tmp_Tag == "h"){ tmp_Flag = 0; }
          if (tmp_Tag == "tn_w"){ tmp_Flag = 0; }
          if (tmp_Tag == "tn_h"){ tmp_Flag = 0; }
          if (tmp_Tag == "tim"){ tmp_Flag = 0; }
          if (tmp_Tag == "time"){ tmp_Flag = 0; }
          if (tmp_Tag == "fsize"){ tmp_Flag = 0; }
          if (tmp_Tag == "resto"){ tmp_Flag = 0; }
          if (tmp_Tag == "bumplimit"){ tmp_Flag = 0; }
          if (tmp_Tag == "imagelimit"){ tmp_Flag = 0; }
          if (tmp_Tag == "replies"){ tmp_Flag = 0; }
          if (tmp_Tag == "images"){ tmp_Flag = 0; }
          if (tmp_Tag == "unique_ips"){ tmp_Flag = 0; }
          if (tmp_Tag == "tail_size"){ tmp_Flag = 0; }
          if (tmp_Tag == "isReplyTo"){ tmp_Flag = 2; }
               
          //If flag is 0 then it is a normal string, if it is a 1 then it is a number.
          if (tmp_Flag == 0)
          {
               while(!((tmp_Chunk[1] == '\"') && (tmp_Chunk[2] == ':')))
               {
                    //Get previous character.
                    tmp_Chunk[0] = tmp_Chunk[1];
                    tmp_Chunk[1] = tmp_Chunk[2];
                    
                    //Get current character.
                    tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
                    
                    tmp_EIP++;
                    if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
               }
          }
          else if (tmp_Flag == 1)
          {
               while(!((tmp_Chunk[0] == '\"') && (tmp_Chunk[1] == ':') && (tmp_Chunk[2] == '\"')))
               {
                    //Get previous character.
                    tmp_Chunk[0] = tmp_Chunk[1];
                    tmp_Chunk[1] = tmp_Chunk[2];
                    
                    //Get current character.
                    tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
                    
                    tmp_EIP++;
                    if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
               }
          }
          else if (tmp_Flag == 2)
          {
               while(!((tmp_Chunk[0] == '\"') && (tmp_Chunk[1] == ':') && (tmp_Chunk[2] == '[')))
               {
                    //Get previous character.
                    tmp_Chunk[0] = tmp_Chunk[1];
                    tmp_Chunk[1] = tmp_Chunk[2];
                    
                    //Get current character.
                    tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
                    
                    tmp_EIP++;
                    if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
               }
          }
          
          //Get previous character.
          tmp_Chunk[0] = tmp_Chunk[1];
          tmp_Chunk[1] = tmp_Chunk[2];
          
          //Get current character.
          tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
          tmp_EIP++;
          if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
          
          //The deliminators are based on the flags state.
          if (tmp_Flag == 0){ tmp_Delim = ','; } else { tmp_Delim = '\"'; }
          
          //If the flag says it is the linked tag then it needs processed differently.
          if (tmp_Flag != 2)
          {
               while(!((tmp_Chunk[2] == tmp_Delim) && (tmp_Chunk[1] != '\\')))
               {
                    //Check for delim
                    if (!((tmp_Chunk[2] == tmp_Delim) && (tmp_Chunk[1] != '\\')))
                    {
                         tmp_Tag_Data += tmp_Chunk[2];
                    }
                    
                    //Get previous character.
                    tmp_Chunk[0] = tmp_Chunk[1];
                    tmp_Chunk[1] = tmp_Chunk[2];
                    
                    //Get current character.
                    tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
                    
                    
                    tmp_EIP++;
                    if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
               }
          }
          else
          {
               tmp_Tag_Data = "";
               while(tmp_Chunk[2] != ']')
               {
                    //Check for delim
                    if (tmp_Chunk[2] != ']')
                    {
                         //If it's a number then add it to the tmp_Data.
                         //If it't not a number set the tmp_ID to NULL.
                         if (is_Num(tmp_Chunk[2]))
                         {
                              //it to the registry.
                              tmp_Tag_Data += tmp_Chunk[2];
                         }
                         else
                         {
                              if ((is_Num(tmp_Chunk[1])))
                              {
                                   add_Citation(tmp_Tag_Data);
                                   tmp_Tag_Data = "";
                              }
                         }
                    }
                    
                    
                    //Get previous character.
                    tmp_Chunk[0] = tmp_Chunk[1];
                    tmp_Chunk[1] = tmp_Chunk[2];
                    
                    //Get current character.
                    tmp_Chunk[2] = raw_Post_Data[tmp_EIP];
                    
                    
                    tmp_EIP++;
                    if (((unsigned int) tmp_EIP) >= raw_Post_Data.size()){ return 0; }
               }
          }
          return 1;
     }
     
     //Parses the raw comment into its componets.
     void parse()
     {
          //cout << "\n\n\n";
          while (get_Next_Tag())
          {
               //cout << "\n\t\t" << tmp_Tag << "|" << tmp_Tag_Data;
               /* RAW JSON
               if (tmp_Tag == "no"){ NO = tmp_Tag_Data; }
               if (tmp_Tag == "now"){ NOW = tmp_Tag_Data; }
               if (tmp_Tag == "name"){ NAME = tmp_Tag_Data; }
               if (tmp_Tag == "com"){ COM = tmp_Tag_Data; }
               if (tmp_Tag == "filename"){ FILENAME = tmp_Tag_Data; }
               if (tmp_Tag == "ext"){ EXT = tmp_Tag_Data; }
               if (tmp_Tag == "w"){ W = tmp_Tag_Data; }
               if (tmp_Tag == "h"){ H = tmp_Tag_Data; }
               if (tmp_Tag == "tn_w"){ TN_W = tmp_Tag_Data; }
               if (tmp_Tag == "tn_h"){ TN_H = tmp_Tag_Data; }
               if (tmp_Tag == "tim"){ TIM = tmp_Tag_Data; }
               if (tmp_Tag == "time"){ TIME = tmp_Tag_Data; }
               if (tmp_Tag == "md5"){ MD5 = tmp_Tag_Data; }
               if (tmp_Tag == "fsize"){ FSIZE = tmp_Tag_Data; }
               if (tmp_Tag == "resto"){ RESTO = tmp_Tag_Data; }
               if (tmp_Tag == "id"){ ID = tmp_Tag_Data; }
               if (tmp_Tag == "country"){ COUNTRY = tmp_Tag_Data; }
               if (tmp_Tag == "country_name"){ COUNTRY_NAME = tmp_Tag_Data; }
               if (tmp_Tag == "m_img"){ M_IMG = tmp_Tag_Data; }
               if (tmp_Tag == "bumplimit"){ BUMPLIMIT = tmp_Tag_Data; }
               if (tmp_Tag == "imagelimit"){ IMAGELIMIT = tmp_Tag_Data; }
               if (tmp_Tag == "replies"){ REPLIES = tmp_Tag_Data; }
               if (tmp_Tag == "images"){ IMAGES = tmp_Tag_Data; }
               if (tmp_Tag == "unique_ips"){ UNIQUE_IPS = tmp_Tag_Data; }
               if (tmp_Tag == "tail_size"){ TAIL_SIZE = tmp_Tag_Data; }
               */
               /*
               //ManBungo pulled data.
               if (tmp_Tag == "no"){ NO = tmp_Tag_Data; }
               if (tmp_Tag == "date"){ NOW = tmp_Tag_Data; }
               if (tmp_Tag == "name"){ NAME = tmp_Tag_Data; }
               if (tmp_Tag == "comment"){ COM = tmp_Tag_Data; }
               if (tmp_Tag == "media_url"){ FILENAME = tmp_Tag_Data; }
               if (tmp_Tag == "ext"){ EXT = tmp_Tag_Data; }
               if (tmp_Tag == "w"){ W = tmp_Tag_Data; }
               if (tmp_Tag == "h"){ H = tmp_Tag_Data; }
               if (tmp_Tag == "tn_w"){ TN_W = tmp_Tag_Data; }
               if (tmp_Tag == "tn_h"){ TN_H = tmp_Tag_Data; }
               if (tmp_Tag == "tim"){ TIM = tmp_Tag_Data; }
               if (tmp_Tag == "time"){ TIME = tmp_Tag_Data; }
               if (tmp_Tag == "md5"){ MD5 = tmp_Tag_Data; }
               if (tmp_Tag == "fsize"){ FSIZE = tmp_Tag_Data; }
               if (tmp_Tag == "resto"){ RESTO = tmp_Tag_Data; }
               if (tmp_Tag == "hashName"){ ID = tmp_Tag_Data; }
               if (tmp_Tag == "country"){ COUNTRY = tmp_Tag_Data; }
               if (tmp_Tag == "country_name"){ COUNTRY_NAME = tmp_Tag_Data; }
               if (tmp_Tag == "m_img"){ M_IMG = tmp_Tag_Data; }
               if (tmp_Tag == "bumplimit"){ BUMPLIMIT = tmp_Tag_Data; }
               if (tmp_Tag == "imagelimit"){ IMAGELIMIT = tmp_Tag_Data; }
               if (tmp_Tag == "replies"){ REPLIES = tmp_Tag_Data; }
               if (tmp_Tag == "images"){ IMAGES = tmp_Tag_Data; }
               if (tmp_Tag == "unique_ips"){ UNIQUE_IPS = tmp_Tag_Data; }
               if (tmp_Tag == "tail_size"){ TAIL_SIZE = tmp_Tag_Data; }
               */
               //Rabbithole pulled data
               //For the OP thread.
               if (tmp_Tag == "topic"){ Topic = tmp_Tag_Data; }
               
               if (tmp_Tag == "no"){ NO = tmp_Tag_Data; }
               if (tmp_Tag == "date"){ NOW = tmp_Tag_Data; }
               if (tmp_Tag == "name"){ NAME = tmp_Tag_Data; }
               if (tmp_Tag == "comment"){ COM = tmp_Tag_Data; }
               if (tmp_Tag == "filename"){ FILENAME = tmp_Tag_Data; }
               if (tmp_Tag == "url"){ URL = tmp_Tag_Data; }
               if (tmp_Tag == "ext"){ EXT = tmp_Tag_Data; }
               if (tmp_Tag == "w"){ W = tmp_Tag_Data; }
               if (tmp_Tag == "h"){ H = tmp_Tag_Data; }
               if (tmp_Tag == "tn_w"){ TN_W = tmp_Tag_Data; }
               if (tmp_Tag == "tn_h"){ TN_H = tmp_Tag_Data; }
               if (tmp_Tag == "tim"){ TIM = tmp_Tag_Data; }
               if (tmp_Tag == "time"){ TIME = tmp_Tag_Data; }
               if (tmp_Tag == "md5"){ MD5 = tmp_Tag_Data; }
               if (tmp_Tag == "fsize"){ FSIZE = tmp_Tag_Data; }
               if (tmp_Tag == "resto"){ RESTO = tmp_Tag_Data; }
               if (tmp_Tag == "hashName"){ ID = tmp_Tag_Data; }
               if (tmp_Tag == "country"){ COUNTRY = tmp_Tag_Data; }
               if (tmp_Tag == "country_name"){ COUNTRY_NAME = tmp_Tag_Data; }
               if (tmp_Tag == "m_img"){ M_IMG = tmp_Tag_Data; }
               if (tmp_Tag == "bumplimit"){ BUMPLIMIT = tmp_Tag_Data; }
               if (tmp_Tag == "imagelimit"){ IMAGELIMIT = tmp_Tag_Data; }
               if (tmp_Tag == "replies"){ REPLIES = tmp_Tag_Data; }
               if (tmp_Tag == "images"){ IMAGES = tmp_Tag_Data; }
               if (tmp_Tag == "unique_ips"){ UNIQUE_IPS = tmp_Tag_Data; }
               if (tmp_Tag == "tail_size"){ TAIL_SIZE = tmp_Tag_Data; }
          }
     }
     
     //Outputs the post into the console.
     void output()
     {
          cout << "\n    #" << NO;
          cout << "\n\t ___" << NOW;
          cout << "\n\t ___" << NAME;
          cout << "\n\t ___" << COM;
          cout << "\n\t ___" << FILENAME;
          cout << "\n\t ___" << EXT;
          cout << "\n\t ___" << W;
          cout << "\n\t ___" << H;
          cout << "\n\t ___" << TN_W;
          cout << "\n\t ___" << TN_H;
          cout << "\n\t ___" << TIM;
          cout << "\n\t ___" << TIME;
          cout << "\n\t ___" << MD5;
          cout << "\n\t ___" << FSIZE;
          cout << "\n\t ___" << RESTO;
          cout << "\n\t ___" << ID;
          cout << "\n\t ___" << COUNTRY;
          cout << "\n\t ___" << COUNTRY_NAME;
          cout << "\n\t ___" << M_IMG;
          cout << "\n\t ___" << BUMPLIMIT;
          cout << "\n\t ___" << IMAGELIMIT;
          cout << "\n\t ___" << REPLIES;
          cout << "\n\t ___" << IMAGES;
          cout << "\n\t ___" << UNIQUE_IPS;
          cout << "\n\t ___" << TAIL_SIZE;
          
          for (int cou_Index=0;cou_Index<Cited_Count;cou_Index++)
          {
               cout << "\n\t   >>" << Cited[cou_Index];
          }
     }
};


//Yes globals are a dirty method, but they are so convenient.

//Tracks the number of posts though.
int Post_Count;
     
//This contains the post data.
c_Post Posts[5000]; //Should never exceed this number.

//Tracks which nodes are treetops, and which are not.
int Post_Is_Treetop[5000];

//Tracks which leg the current thread is on, it in no way should reach 1000 in length but you never know.
int Thread_Legs[1000];

//Tracks which nodes are currently being evaluated. It does this by saving the node indexes in the posts array.
int Thread[5000];


//Tracks the current thread depth.
int Thread_Depth;

//Tracks whether or not the current top down thread is complete.
int flg_Thread_Not_Done;

//The current number of threads found, updates as it goes in increments of 5.
int Thread_Count;

//Holds the post number as the variable name with the index as the data. This will be used in the backpropagation.
c_TP_Lookup_Tree Post_Lookup_Tree;

//Resets the thread tracking.
void reset_Threads()
{
     for (int cou_Index=0;cou_Index<1000;cou_Index++)
     {
          Thread_Legs[cou_Index] = 0;
          Thread[cou_Index] = 0;
     }
     flg_Thread_Not_Done = true;
     Thread_Depth = 0;
}


//Outputs the current thread.
void output_Current_Thread_json(string p_File)
{
     
     ofstream SF;
     SF.open(p_File, ios::app);
     string tmp_COM = "";
     string tmp_String = "";
     
     if (Thread_Count == 0)
     { 
          SF << "["; 
          SF << "\n{";
          SF << "\n   \"header\":{\"sauce\":\"4chan\",\"topic\":\"" << Topic << "\",\"type\":\"thread\"}";
          SF << "\n},";
     }
     if (Thread_Count != 0){ SF << ","; }
     SF << "\n{\n\"thread_number\":" << Thread_Count << ",";
     SF << "\n \"posts\":[";
     for (int cou_Index=(Thread_Depth - 1);cou_Index>=0;cou_Index--)
     {
          if (Thread[cou_Index] == -1){ continue; }
          SF <<   "\n   {";
          //cout << "\n    " << Thread[cou_Index] << "    NO:" << Posts[Thread[cou_Index]].NO << "    COM:" << Posts[Thread[cou_Index]].COM;
          SF << "\"id\":\"" << Posts[Thread[cou_Index]].NO << "\",";
          SF << "\"date\":\"" << Posts[Thread[cou_Index]].NOW << "\",";
          SF << "\"name\":\"" << Posts[Thread[cou_Index]].NAME << "\",";
          SF << "\"country\":\"" << Posts[Thread[cou_Index]].COUNTRY << "\",";
          if (Posts[Thread[cou_Index]].FILENAME != ""){ SF << "\"media_urls\":[{\"url\":\"" << Posts[Thread[cou_Index]].URL << "\",\"filename\":\"" << Posts[Thread[cou_Index]].FILENAME << "\"}],"; }
          SF << "\"comment\":\"" << Posts[Thread[cou_Index]].COM << "\"";
          SF << "}";
          if (cou_Index != 0){ SF << ","; }
     }
     SF << "\n   ]";
     SF << "\n}";
     
     SF.close();
}

void output_Current_Thread(string p_File)
{
     if (settings_Output_Format == 1){ output_Current_Thread_json(p_File); }
}

//Tracks a thread from the given post.
//Outputs it to the given output file, if none defaults to training.txt
void track_Thread(int p_Tt, string p_File = "training.txt")
{
     reset_Threads();
     
     //Get the current post and go with it.
     Thread[0] = p_Tt;
     Thread_Legs[0] = 0;
     Thread_Depth = 1;
     
     while(flg_Thread_Not_Done && (Thread_Count < settings_Thread_Cap))
     {
          if (Thread_Depth <= 0)
          {
               flg_Thread_Not_Done = false;
               return;
          }
          
          //Explore current leg, if none the end of the thread has been reached.
          if (Posts[Thread[Thread_Depth - 1]].Cited_Count > Thread_Legs[Thread_Depth - 1])
          {
               flg_Thread_Not_Done = true;
               
               //We are not done because there are still legs on this node.
               //if (Posts[Thread[Thread_Depth - 1]].Cited_Count > (Thread_Legs[Thread_Depth - 1] + 1))
               
               //Explore the lower leg.
               Thread[Thread_Depth] = Post_Lookup_Tree.get_int(Posts[Thread[Thread_Depth - 1]].Cited[Thread_Legs[Thread_Depth - 1]]);
               Thread_Legs[Thread_Depth] = 0;
               Thread_Depth++;
          }
          else
          {
               //Are we at the bottom of the thread?
               if (Posts[Thread[Thread_Depth - 1]].Cited_Count == 0)
               {
                    if (Thread_Depth >= settings_Min_Thread_Length){ output_Current_Thread(p_File); Thread_Count++; }
                    
               }
               
               //Reverse course, head upriver.
               Thread[Thread_Depth - 1] = 0;
               Thread_Legs[Thread_Depth - 1] = 0;
               Thread_Depth--;
               Thread_Legs[Thread_Depth - 1]++;
          }
     }
     
}

//Tracks all the treetop threads.
void track_Threads(string p_Output_Filename)
{
     cout << " Number_Of_Posts:" << Post_Count;
     Thread_Count = 0;
     
     //If a post is a treetop we will retroactively track all of the threads leading to it.
     for (int cou_Index=0;cou_Index<Post_Count;cou_Index++)
     {
          if (Post_Is_Treetop[cou_Index])
          {
               track_Thread(cou_Index, p_Output_Filename);
          }
     }
     
     //If there are threads open the file and finalize it with ']'
     if (Thread_Count != 0)
     {
          ofstream SF;
          SF.open(p_Output_Filename, ios::app);
          
          if (SF.is_open())
          {
               SF << "\n]";
          }
          SF.close();
     } 
     cout << " Thread_Count:" << Thread_Count;
}

//Find the threads and output them to the file.
void find_Treetops()
{
     int tmp_CC = 0;
     for (int cou_Index=0;cou_Index<Post_Count;cou_Index++)
     {
          //Get the number of citations in this thread.
          tmp_CC = Posts[cou_Index].Cited_Count;
          
          //If none then continue;
          if (tmp_CC == 0){ continue; }
          
          //cout << "\n  Cited " << Posts[cou_Index].Cited_Count;
          
          //Loop through each citation and set the treetop flags for the linked node to false.
          for (int cou_C=0;cou_C<tmp_CC;cou_C++)
          {
               //cout << "\n   " << cou_C << " " << Posts[cou_Index].Cited[cou_C];
               Post_Is_Treetop[Post_Lookup_Tree.get_int(Posts[cou_Index].Cited[cou_C])] = false;
          }
          //system("PAUSE > NULL");
     }
}



void thread_Parser(string p_Filename, string p_Output_Filename)
{
     cout << "\n" << p_Filename;
     ifstream SF;
     Post_Count = 0;
     
     for (int cou_Index=0;cou_Index<1000;cou_Index++)
     {
          Post_Is_Treetop[cou_Index] = 1;
          Posts[cou_Index].Post_ID = cou_Index;
     }
     
     reset_Threads();
     
     char tmp_Chunk[3];
     tmp_Chunk[0] = ' ';
     tmp_Chunk[1] = ' ';
     tmp_Chunk[2] = ' ';
     
     char tmp_Junk = ' ';
     
     SF.open(p_Filename.c_str());
     if (!SF.is_open()){ cout << "\n\n Error Could Not Open File " << p_Filename << "!!!"; return; }
     
     //Gather the topic for the header.
     for (int cou_Index=0;cou_Index<11;cou_Index++)
     {
          tmp_Chunk[0] = tmp_Chunk[1];
          tmp_Chunk[1] = tmp_Chunk[2];
          tmp_Chunk[2] = SF.get();
     }
     
     while(tmp_Chunk[2] != '\"')
     {
          Topic += tmp_Chunk[2];
          tmp_Chunk[0] = tmp_Chunk[1];
          tmp_Chunk[1] = tmp_Chunk[2];
          tmp_Chunk[2] = SF.get();
     }
     cout << " Topic->" << Topic;
     
     while (!SF.eof())
     {
          tmp_Chunk[0] = tmp_Chunk[1];
          tmp_Chunk[1] = tmp_Chunk[2];
          tmp_Chunk[2] = SF.get();
          
          
          if (((tmp_Chunk[0] == 'l') && (tmp_Chunk[1] == ',') && (tmp_Chunk[2] == '\"')) || ((tmp_Chunk[0] == '}') && (tmp_Chunk[1] == ',') && (tmp_Chunk[2] == '\"')) || ((tmp_Chunk[1] == '{') && (tmp_Chunk[2] == '\"')))
          {
               Post_Count++;
               
               //To prevent bounding errors, on those rare threads that may reach this length it will break the conditioning and the rest of the thread will not be evaluated. :(
               if (Post_Count >= 5000){ Post_Count = 4999; break; }
               
               for (int cou_Index=0;cou_Index<9;cou_Index++)
               {
                    Posts[Post_Count - 1].NO += SF.get();
               }
               
               //To remove ":{
               tmp_Junk = tmp_Chunk[0];
               tmp_Chunk[0] = SF.get();
               tmp_Chunk[0] = SF.get();
               tmp_Chunk[0] = SF.get();
               tmp_Chunk[0] = tmp_Junk;
          }
          else
          {
               if (Post_Count > 0)
               {
                    Posts[Post_Count - 1].raw_Post_Data += tmp_Chunk[2];
               }
          }
     }
     SF.close();
     
     //Parse the posts through the .parse member.
     for (int cou_Index=0;cou_Index<Post_Count;cou_Index++)
     {
          //cout << "\n --== " << cou_Index << " ==--";
          Posts[cou_Index].parse();
          Post_Lookup_Tree.set_int((Posts[cou_Index].NO), cou_Index);
          //Posts[cou_Index].output();
     }
     
     find_Treetops();
     
     track_Threads(p_Output_Filename);
}



int main(int argc, char **argv)
{
     load_Settings();
     
     //thread_Parser("TData/4Chan/tst.json", "TData/4Chan/tst.json.txt");
     //system("PAUSE > NULL");
     //return 1;
     
     if (argc == 3)
     {
          thread_Parser(argv[1], argv[2]);
     }
     if (argc == 2)
     {
          string tmp_Dest = ((string) argv[1]) + ".txt";
          thread_Parser(argv[1], tmp_Dest);
     }
     return 1;
}












