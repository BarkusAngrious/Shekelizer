@echo off
echo  --==   Shekelizer 4Chan Archive Scraper v2.0
echo  --==   Copyright (c) 2018 /BTB/
echo  --==   Use of this source code is governed by /pol/
echo  --==   Unauthorized usage of this code will result in death by a thousand cuts
echo.
echo.
echo.

rem Create the directories if they do not already exist.
md Archive
md Archive.Old
md Parsed

rem Add a newline to the Gathered_Threads.dat file, this will create the file if it does not exist
echo.>>Gathered_Threads.dat

rem Scrape the threads.
rabbithole.py

rem Parse the files
echo.>runrun.bat
echo Gathering files to parse (may take several minutes)...
for /r "archive" %%I in (*.*) do echo Shekel_Sorter "Archive\%%~nxI" "Parsed\%%~nI.txt">>runrun.bat
echo Finished gathering files...
echo Parsing files...
call runrun.bat

rem Now that we are done with them move all the archived files to the old folder.
echo Moving archived files to the archive.old folder.
for /r "archive" %%I in (*.*) do copy "Archive\%%~nxI" "Archive.Old\%%~nxI"

echo Purging files from Archive.
for /r "archive" %%I in (*.*) do del "Archive\%%~nxI"

rem Wipe the runrun.bat.
echo.>runrun.bat
pause