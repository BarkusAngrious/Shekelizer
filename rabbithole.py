print(" --==   Copyright (c) 2018 /BTB/")
print(" --==   Use of this source code is governed by /pol/")
print(" --==   Unauthorized usage of this code will result in death by a thousand cuts\n\n\n")
##Notes
## File "Gathered_Threads.dat" must exist in the dir with this file.
## Dir "Archive" must also exist in the dir this file is in.

import urllib
import urllib.request
import json
import html

##This is to get around some websites denying download from bots by saying it is Mozilla 5.0.
class AppURLopener(urllib.request.FancyURLopener):
    version = "Mozilla/5.0"
    timeout = 5

opener = AppURLopener()

##Load the thread tracking file into tmp_Gathered_Threads
load_File = open("Gathered_Threads.dat")
if load_File.closed != 1:
    tmp_Gathered_Threads_Data = load_File.read()
    tmp_Gathered_Threads = tmp_Gathered_Threads_Data.split("\n")

##Close the file, we are done here.
load_File.close()

##Get the archives page to determine threads to download
print("Getting Archives...")
archraw = opener.open("http://a.4cdn.org/pol/archive.json")

##Parse the archive.json to recover all thread IDs in the tmp_Componets dictionary
print("Gathering Thread IDs...")
archdata = archraw.read()
archp = str(archdata)
archp = archp.replace("b'[", "")
archp = archp.replace("]'", "")
tmp_Componets = archp.split(',')

##Close the opener.
opener.close()

##Prepare the thread url and filename to be tacked onto the thread IDs.
threadhead = "http://boards.4chan.org/pol/thread/"
filehead = "Archive/polThread-"

##Gather the data from every thread found in the archive.
print("Gathering Threads...")
for cou_Thread in tmp_Componets:

    ##Check if the thread has already been gathered.
    if cou_Thread in tmp_Gathered_Threads:
        print("FOUND " + cou_Thread + "!!!")
        continue

    ##Concatinate the thread url to pull and the filename to put it in.
    threadurl = threadhead + str(cou_Thread) + ".json"
    filename = filehead + str(cou_Thread) + ".json"
    print("\n" + threadurl)
    
    ##Get the page data.
    tst = opener.open(threadurl)
    
    #If it is not open do nothing.
    if tst.closed != 1:
        
        ##Add the current thread to the Gathered_Threads.dat
        gt_Save_File = open("Gathered_Threads.dat", "a")
        gt_Save_File.write("\n" + cou_Thread)
        gt_Save_File.close()
        
        #Gather the tst data into tmpdata to load into tmpjson.
        tmpdata = tst.read()
        tstjson = json.loads(tmpdata)
        
        ##If you want to see the json loaded uncomment this.
        ##print(json.dumps(tstjson, indent=4))
        
        ##Open the file to put the thread info in.
        save_file = open(filename, "w", encoding='utf-8')
        
        
        save_file.write("{")
        for cou_Post in tstjson["posts"]:
            if not cou_Post:
                continue
            ##print("\n\n\n  --==========--")
            ##print("\n --==  POST  ==--")
            ##print("\n  --==========--")
            if "replies" in cou_Post:
                if cou_Post["replies"] == 0:
                    continue
                
            if "semantic_url" in cou_Post:
                print("   --\"topic\":\"" + cou_Post["semantic_url"] + "\"")
                save_file.write("\"topic\":\"" + cou_Post["semantic_url"] + "\"")
            
            if "no" in cou_Post:
                save_file.write(",\"" + str(cou_Post["no"]) + "\":{")
                
            if "time" in cou_Post:
                save_file.write("\"date\":" + str(cou_Post["time"]))
                
            if "name" in cou_Post:
                save_file.write(",\"name\":\"" + cou_Post["name"] + "\"")
                
            if "country_name" in cou_Post:
                save_file.write(",\"country\":\"" + cou_Post["country_name"] + "\"")
                
            if "tim" in cou_Post:
                ##Save the media urls as raw tags, at this time I will just pull the image url and filename from the post, not from the comment.
                save_file.write(",\"url\":\"http://i.4cdn.org/pol/" + str(cou_Post["tim"]) + cou_Post["ext"] + "\",\"filename\":\"" + cou_Post["filename"] + cou_Post["ext"] + "\"")
                
            if "com" in cou_Post:
                ##Cleanse the post.
                tmpcom = ((html.unescape(cou_Post["com"])).replace("<br>", "\\n"))
                tmpcom = tmpcom.replace("<span class=\"quote\">", "")
                tmpcom = tmpcom.replace("</span>", "")
                tmpcom = tmpcom.replace("</a>", "")
                tmpcom = tmpcom.replace("\"", "\\\"")
                tmpcoml = ""
                tmpcomr = ""
                tmpreplyto = ""
                
                ##This flag is so that if there is a reply we can place the closing bracket.
                flg_isreply = False

                ##If a hyperlink tag is found then start the isReplyTo array.
                if tmpcom.find("<a href=\\\"#p") != -1:
                    save_file.write(",\"isReplyTo\":[")
                    flg_isreply = True
                ##While there are hyperlink tags to be found parse them into 
                while tmpcom.find("<a href=\\\"#p") != -1:
                    tmpcoml = tmpcom[:tmpcom.find("<a href")]
                    tmpcomr = tmpcom[(tmpcom.find("<a href") + 55):]
                    tmpreplyto = tmpcom[(tmpcom.find("<a href") + 12):(tmpcom.find("<a href") + 21)]
                    tmpcom = tmpcoml + tmpcomr
                    save_file.write("\"" + str(tmpreplyto) + "\"")
                    if tmpcom.find("<a href") != -1:
                          save_file.write(",")
                if flg_isreply == True:
                    save_file.write("]")
                save_file.write(",\"comment\":\"" + tmpcom + "\"")
            save_file.write("}")
        save_file.write("}")
        save_file.close()
        
    opener.close()